

//import express module
const express = require ('express');

//express () server
const app = express()
const PORT = 4000;

//middlewares
app.use(express.json())
app.use(express.urlencoded ({extended:true}))


const user = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	}
]

app.get("/home", (req, res) => res.status(200).send("Welcome to the home page"))

app.get("/users", (req, res) => res.status(200).send (user))


app.delete(`/delete-users`, (res, req) => res.send (`User johndoe has been deleted.`))


app.listen(PORT, () => console.log(`Server Connected to ${PORT}`))